<?php session_start();
if (isset($_GET['etat'])) {
	session_destroy();
}elseif (isset($_SESSION['partie_en_cours'])) {
	header("location:jeuTestLogique/index.php");
}

?>

<?php

if (isset($_GET['id'])) {
	
	$idvar=$_GET['id'];
	
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_URL => "http://34.198.49.30:5000/$idvar/logique",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => false,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_POSTFIELDS =>"{\n\t\"score\": 52\n}",
	CURLOPT_HTTPHEADER => array(
	"Content-Type: application/json"
	),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	$response=strval($response);
	if ($err)
	{
		echo "cURL Error #:" . $err;
		// lien à changer pour renvoyer vers la page de derrick
		header("location:../index.php");
	} 
	else
	{
	if ($response[10]!="4")
	{
		setcookie('id',$idvar,time() + 7200);
	}
		
		 
	}

}
else
{
	// lien à changer pour renvoyer vers la plateforme
	 header("location:../index.php");
}

?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Cerco</title>
<!-- 



-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400">  <!-- Google web font "Open Sans" -->
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  
  <link rel="stylesheet" href="css/demo.css" />
  <link rel="stylesheet" href="css/templatemo-style.css">
  
  <script type="text/javascript" src="js/modernizr.custom.86080.js"></script>
		
	</head>

	<body>

			<div id="particles-js"></div>
		
			<ul class="cb-slideshow">
	            <li></li>
	            <li></li>
	            <li></li>
	            <li></li>
	            <li></li>
	            <li></li>
	        </ul>

			<div class="container-fluid">
				<div class="row cb-slideshow-text-container ">
					<div class= "tm-content col-xl-6 col-sm-8 col-xs-8 ml-auto section">
					        <header class="mb-5"><h1>CERCO 43 | JEUX</h1></header>
					           
					
                    <div class="container">
               	    	  <div class="col-md-5 col-sm-5 col-xs-5">
                              <button  onclick="window.location.href='jeuTestLogique/index.php'" class="tm-btn-subscribe">Commencer</button>	
						  </div>
                    </div>
                    </div>
				</div>	

			</div>


			<div class="footer-link">

				<div class="tm-social-icons-container text-xs-center">
	                    <a href="#" class="tm-social-link"><i class="fa fa-facebook"></i></a>
	                    <a href="#" class="tm-social-link"><i class="fa fa-google-plus"></i></a>
	                    <a href="#" class="tm-social-link"><i class="fa fa-twitter"></i></a>
	                    <a href="#" class="tm-social-link"><i class="fa fa-linkedin"></i></a>
	                </div>

					
				</div>	
	</body>

	<script type="text/javascript" src="js/particles.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
</html>